#!/bin/sh

for SECRET_TYPE in ${SECRETS_TYPES}; do   
    if [ -z ${VAULT_TOKEN+x} ]; then    # check if var $VAULT_TOKEN is not set
        export VAULT_TOKEN="$(vault write -field=token auth/jwt/login role=${PROJECT_NAME}-${SECRET_TYPE} jwt=$CI_JOB_JWT)"
    fi

    case "${SECRET_TYPE}" in
        ssh-keys|certs)
            mkdir -p "${SECRET_TYPE}"
            for SECRET_NAME in $( vault kv list "lms-secrets/${PROJECT_NAME}/${SECRET_TYPE}/" | tail -n +3 ); do
                vault kv get -field="${SECRET_NAME}" "lms-secrets/${PROJECT_NAME}/${SECRET_TYPE}/${SECRET_NAME}" > "./${SECRET_TYPE}/${SECRET_NAME}"
            done
            ;;
        *)
            for SECRET_NAME in $( vault kv list "lms-secrets/${PROJECT_NAME}/${SECRET_TYPE}/" | tail -n +3 ); do
                PASSWORD="$(vault kv get -field=password lms-secrets/${PROJECT_NAME}/${SECRET_TYPE}/${SECRET_NAME})"
                echo "SECRET_NAME=${PASSWORD}" >> "${CI_JOB_NAME}.env"
            done              
            ;;
    esac
done